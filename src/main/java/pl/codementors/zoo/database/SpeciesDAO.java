package pl.codementors.zoo.database;

import pl.codementors.zoo.model.Species;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by psysiu on 6/28/17.
 */
public class SpeciesDAO extends BaseDAO<Species> {

    private String[] columns = {"name"};

    @Override
    public String getTableName() {
        return "species";
    }

    @Override
    public Species parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        return new Species(id, name);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Species value) {
        Object[] values = {value.getName()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Species value) {
        return value.getId();
    }

    public Species create(String name) {
        return new Species(name);
    }

    public Species findByName(String name) {
        String sql = "SELECT * FROM species WHERE name = ?";
        Object[] params = {name};
        return executeQuery(sql, params).get(0);
    }

}
