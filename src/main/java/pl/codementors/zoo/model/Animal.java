package pl.codementors.zoo.model;

import lombok.*;

/**
 * Created by psysiu on 6/29/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Animal {

    public enum Gender {male, female;}

    private int id;

    private String name;

    private int age;

    private Gender gender;

    private Breeder breeder;

    private Species species;

    private Enclosure enclosure;

    public Animal(String name, int age, Gender gender, Breeder breeder, Species species, Enclosure enclosure) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.breeder = breeder;
        this.species = species;
        this.enclosure = enclosure;
    }
}
