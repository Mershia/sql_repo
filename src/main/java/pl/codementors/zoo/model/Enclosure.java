package pl.codementors.zoo.model;

import lombok.*;

/**
 * Created by psysiu on 6/29/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Enclosure {

    private int id;

    private String name;

    private int width;

    private int height;

    private Species species;

    public Enclosure(String name, int width, int height, Species species) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.species = species;
    }
}
